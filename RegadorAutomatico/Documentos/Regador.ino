#include <DHT.h>

int pinLDR = A0;
int pinDHT = 14;
int pinHum = D1;
int pinRele= D2;

int luminosidad;
int humedadSuelo;
float temperatura;
float humedad;
float h,t;
int error=0;
int n=0;
unsigned long inicio;
String comando;
int regadoManual=0;
bool regar=false;

#define DHTTYPE  DHT11
DHT dht(pinDHT, DHTTYPE);

void setup() {
  Serial.begin(9600);
  pinMode(pinLDR,INPUT);
  pinMode(pinRele,OUTPUT);
  pinMode(pinHum,INPUT);
  EncenderRelevador(false);
  inicio=millis();
}

void loop() {
  ObtenerComando();
  if(millis()-inicio>5000){
    inicio=millis(); 
    if(regadoManual==1){
      regar=true;
    }else{
      regar=false;
      if(ObtenerLuminosidad()<10){
        if(ObtenerHumedad()==0){
          regar=regar ||true;
        }else{
          regar=regar||false;
        }
      }else{
        regar=regar||false;
      }
      ObtenerTemperaturaHumedad();
    }
    Serial.print("L=");          
    Serial.print(luminosidad);
    Serial.print(" HS=");        
    Serial.print(humedadSuelo);
    Serial.print(" T=");         
    Serial.print(temperatura);
    Serial.print(" H=");         
    Serial.print(humedad);
    Serial.print(" RM=");        
    Serial.print(regadoManual);
    Serial.print(" N=");        
    Serial.print(n);
    Serial.print(" Error=");    
    Serial.print(error);
    EncenderRelevador(regar);
  }
}

void EncenderRelevador(bool encender){
  if(encender){
    digitalWrite(pinRele,HIGH);
    Serial.println(" R=1");
  }
  else{
    digitalWrite(pinRele,LOW);
    Serial.println(" R=0");
  }
}

int ObtenerLuminosidad(){
  luminosidad=map(analogRead(pinLDR),0,1024,0,100);
  return luminosidad;
}

int ObtenerHumedad(){
  humedadSuelo=digitalRead(pinHum);
  return humedadSuelo;
}

void ObtenerTemperaturaHumedad(){
  n++;
  h= dht.readHumidity();
  t = dht.readTemperature();
  if (isnan(h) || isnan(t)) {
    error++;
  }else{
    temperatura=t;
    humedad=h;
  }
}

void ObtenerComando(){
  comando="";
  char letra;
  if(Serial.available()>0){
    comando=Serial.readString();
    comando=comando.substring(0,comando.length()-1);
    Serial.println("Comando: [" + comando + "]");
  }
  if(comando=="R1"){
    regadoManual=1;
    Serial.println("Regado Manual Activado");
  }
  if(comando=="R0"){
    regadoManual=0;
    Serial.println("Regado Manual Desactivado");
  }
}
