﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace RegadorAutomatico
{
    public partial class MainWindow : Window
    {
        PuertoCOM puerto;
        List<Lectura> lecturas;
        string mensaje = "";
        DispatcherTimer timerMensaje;
        DispatcherTimer timerActualiza;
        Lectura lecturaActual;

        public MainWindow()
        {
            InitializeComponent();
            puerto = new PuertoCOM();
            puerto.DatoRecibido += Puerto_DatoRecibido;
            puerto.Error += Puerto_Error;
            lecturas = new List<Lectura>();
            lstPuertos.ItemsSource = puerto.ObtenlistaDepuertos;
            timerMensaje = new DispatcherTimer();
            timerMensaje.Interval = new TimeSpan(0, 0, 0, 0, 100);
            timerMensaje.Tick += Timer_Tick;
            timerMensaje.Start();

            lecturaActual = null;

            timerActualiza = new DispatcherTimer();
            timerActualiza.Interval = new TimeSpan(0, 0, 0, 2);
            timerActualiza.Tick += TimerActualiza_Tick;
            timerActualiza.Start();
            this.SizeChanged += MainWindow_SizeChanged;
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            plotGrafica.Width = e.NewSize.Width * .95;
        }

        private void TimerActualiza_Tick(object sender, EventArgs e)
        {
            if (lecturaActual != null)
            {
                if (lecturaActual.Regando)
                {
                    lblRegando.Content = "Regando";
                    lblRegando.Background = new SolidColorBrush(Colors.Green);
                }
                else
                {
                    lblRegando.Content = "Sin regar";
                    lblRegando.Background = null;
                    
                }
                if (lecturaActual.RegadoManual)
                {
                    btnActivar.IsEnabled = false;
                    btnDesactivar.IsEnabled = true;
                }
                else
                {
                    btnActivar.IsEnabled = true;
                    btnDesactivar.IsEnabled = false;
                }
                lecturas.Add(lecturaActual);
                dtgDatos.ItemsSource = null;
                dtgDatos.ItemsSource = lecturas;
                lecturaActual = null;

                ActualizaGrafica();
            }

        }

        private void ActualizaGrafica()
        {
            PlotModel model = new PlotModel();
            DateTimeAxis ejeTiempo = new DateTimeAxis() { StringFormat = "hh:mm:ss", Position = AxisPosition.Bottom };
            LineSeries temperatura = new LineSeries();
            LineSeries humedadAmbiental = new LineSeries();
            LineSeries humedadSuelo = new LineSeries();
            LineSeries luminosidad = new LineSeries();
            foreach (var item in lecturas.OrderByDescending(d=>d.FechaHora).Take(50))
            {
                temperatura.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora, item.Temperatura));
                humedadAmbiental.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora, item.HumedadAmbiental));
                humedadSuelo.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora, item.HumedadDeSuelo ? 100f: 0f));
                luminosidad.Points.Add(DateTimeAxis.CreateDataPoint(item.FechaHora, item.Luminosidad));
            }
            temperatura.Title = "Temperatura °C";
            humedadAmbiental.Title = "Humedad ambiental %";
            humedadSuelo.Title = "Hay humedad en suelo";
            luminosidad.Title = "Luminosidad %";
            model.Axes.Add(ejeTiempo);
            model.Series.Add(temperatura);
            model.Series.Add(humedadAmbiental);
            model.Series.Add(humedadSuelo);
            model.Series.Add(luminosidad);
            plotGrafica.Model = model;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (mensaje != "")
            {
                lstLog.Items.Add(mensaje);
                mensaje = "";
            }
        }

        private void Puerto_Error(object sender, string e)
        {
            Notifica(e, true);
        }

        private void Puerto_DatoRecibido(object sender, string e)
        {
            if (e.Contains("Regado"))
            {
                Notifica(e.Replace("\r",""), false);
            }
            if (e.Length > 45)
            {
                try
                {
                    lecturaActual = new Lectura(e);
                }
                catch (Exception ex)
                {
                    Notifica(ex.Message, true);
                }
            }
        }

        private void Notifica(string message, bool esError)
        {
            mensaje = (esError ? "Error: " : " ") + message;
        }



        private void chkConectar_Checked(object sender, RoutedEventArgs e)
        {
            if (puerto.Conectar(lstPuertos.Text, 9600))
            {
                Notifica("Puerto conectado", false);
            }
        }

        private void chkConectar_Unchecked(object sender, RoutedEventArgs e)
        {
            if (puerto.Deconectar())
            {
                Notifica("Puerto desconectado", false);
            }
        }

        private void btnActivar_Click(object sender, RoutedEventArgs e)
        {
            puerto.EnviarDatos("R1");
        }

        private void btnDesactivar_Click(object sender, RoutedEventArgs e)
        {
            puerto.EnviarDatos("R0");
        }
    }
}
