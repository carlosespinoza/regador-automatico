﻿using System;

namespace RegadorAutomatico
{
    public class Lectura
    {
        public DateTime FechaHora { get; set; }
        public int NumLectura { get; set; }
        public float Temperatura { get; set; }
        public float HumedadAmbiental { get; set; }
        public bool HumedadDeSuelo { get; set; }
        public int Luminosidad { get; set; }
        public bool RegadoManual { get; set; }
        public bool Regando { get; set; }
        public int NumErrores { get; set; }
        public Lectura(string lectura)
        {
            //L=60 HS=1 T=20.00 H=95.00 RM=0 N=83 Error=0 R=0
            string[] lecturas = lectura.Split(new char[] { ' ', '\r','\n' });
            foreach (var item in lecturas)
            {
                string[] datos = item.Split('=');
                switch (datos[0])
                {
                    case "L":
                        Luminosidad = int.Parse(datos[1]);
                        break;
                    case "HS":
                        HumedadDeSuelo = datos[1] == "1";
                        break;
                    case "T":
                        Temperatura = float.Parse(datos[1]);
                        break;
                    case "H":
                        HumedadAmbiental = float.Parse(datos[1]);
                        break;
                    case "RM":
                        RegadoManual = datos[1] == "1";
                        break;
                    case "N":
                        NumLectura = int.Parse(datos[1]);
                        break;
                    case "Error":
                        NumErrores = int.Parse(datos[1]); 
                        break;
                    case "R":
                        Regando = datos[1] == "1";
                        break;
                    default:
                        break;
                }
            }
            FechaHora = DateTime.Now;
        }
    }
}
