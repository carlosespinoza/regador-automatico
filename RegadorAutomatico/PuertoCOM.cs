﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Ports;
using System.Diagnostics;

namespace RegadorAutomatico
{
    public class PuertoCOM
    {
        SerialPort puerto;
        public event EventHandler<string> DatoRecibido;
        public event EventHandler<string> Error;
        
        public List<string> ObtenlistaDepuertos { get => SerialPort.GetPortNames().ToList(); }
        
        public bool Conectar(string com, int baudrate)
        {
            try
            {
                puerto = new SerialPort(com, baudrate);
                puerto.Parity = Parity.None;
                puerto.DataBits = 8;
                puerto.StopBits = StopBits.One;
                puerto.DataReceived += Puerto_DataReceived;
                puerto.Open();
                return puerto.IsOpen;
            }
            catch (Exception ex)
            {
                Error(this, ex.Message);
                return false;
            }
        }

        private void Puerto_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string dato= puerto.ReadLine();
            Debug.WriteLine("Recibiendo: "+ dato);
            DatoRecibido(null,dato);
        }

        public bool EnviarDatos(string dato)
        {
            try
            {
                if (puerto != null && puerto.IsOpen)
                {
                    puerto.WriteLine(dato);
                    Debug.WriteLine("Escribiendo: " +dato );
                    return true;
                }
                else
                {
                    Error(this, "El puerto no esta abierto");
                    return false;
                }

            }
            catch (Exception ex)
            {
                Error(this, ex.Message);
                return false;
            }
        }

        public bool Deconectar()
        {
            try
            {
                if (puerto != null && puerto.IsOpen)
                {
                    puerto.Close();
                    Debug.WriteLine("Puerto Desconectado");
                    return true;
                }
                else
                {
                    Error(this, "El puerto no esta abierto...");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Error(this, ex.Message);
                return false;
            }
            
        }
    }
}